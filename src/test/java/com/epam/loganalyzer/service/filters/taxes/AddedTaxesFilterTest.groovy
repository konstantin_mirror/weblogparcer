package com.epam.loganalyzer.service.filters.taxes

import com.epam.loganalyzer.model.tdp.Tax
import com.epam.loganalyzer.service.filters.taxes.AddedTaxesFilter
import spock.lang.Specification

class AddedTaxesFilterTest extends Specification {

    def "Test added taxes"() {
        given: 'filter for the  added taxes'
        AddedTaxesFilter addedTaxesFilter = new AddedTaxesFilter()
        when: ' trying get added tax'
        Set<Tax> taxes = addedTaxesFilter.parser(Collections.singletonList(testLine) as List<String>)
        then: ' trying compare  expected and obtained result'
        with(taxes[0]){
            type == testType
            seqNo == testSeqNo
            code == testCode
            nation == testNation
        }
        where: 'table'
        testLine                                                            || testType | testSeqNo | testCode | testNation
        "2017-03-13 09:21:54,942 [DEBUG] [http-0.0.0.0-8080-6] " +
        "[com.epam.tdp.tax.atpco.ATPCOTaxQuotationServiceImpl]" +
        " Adding TaxRule nation=TW code=TW type=001 sequenceNumber=115000"  || '001'    | '115000'  | 'TW'     | 'TW'
    }
}

