package com.epam.loganalyzer.service.filters.fares

import com.epam.loganalyzer.model.tdp.Fare
import com.epam.loganalyzer.service.filters.fares.RemovedFaresFilter
import spock.lang.Specification


class RemovedFaresFilterTest extends Specification {
    def " test for removed fares"() {
        given: 'removed fares filter'
        RemovedFaresFilter filter = new RemovedFaresFilter()
        Set<Fare> removedfares;
        when: 'parse log line'
        removedfares = filter.parser(Collections.singletonList(logLine) as List<String>)
        then: 'compare expected fare information'
        removedfares[0].clazz == clazz
        removedfares[0].tariff == tariff
        removedfares[0].rule == rule
        removedfares[0].id == id
        where:

        logLine                                           || clazz   | tariff | rule   | id
        'Removing fare UPTHK/912/24PH/CAT25FARE_INTLFARE' || 'UPTHK' | '912'  | '24PH' | 'CAT25FARE_INTLFARE'
    }
}

