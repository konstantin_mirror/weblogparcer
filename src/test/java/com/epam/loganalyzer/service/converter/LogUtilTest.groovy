package com.epam.loganalyzer.service.converter

import com.epam.loganalyzer.model.record.LogRecord
import com.epam.loganalyzer.service.converter.LogUtil
import spock.lang.Specification



class LogUtilTest extends Specification{
    def "Test for  the parse log's lines"(){
        given:"Line"
        def logUtil = new LogUtil()
        when:"Parse line"
        LogRecord logRecord = logUtil.selectElements(logLine)
        then:"result is"
        with(logRecord){
            println(timeStamp)
            timeStamp.time == time
            timeStamp.thread == thread
            timeStamp.clazz == clazz
            timeStamp.type == type
        }
        where:"lines for the tests"
        logLine                                                                                      || type      | time                     | thread               | clazz
        '2017-08-02 12:18:12,985 [DEBUG]' +
                ' [http-0.0.0.0-8080-4] [AdaptorResolverUtils] XML Object is'                        || "DEBUG" | "2017-08-02 12:18:12,985"| "http-0.0.0.0-8080-4"|"AdaptorResolverUtils"
        '2017-08-02 12:18:12,985 [ERROR]' +
                ' [http-0.0.0.0-8080-4] [AdaptorResolverUtils] XML Object is'                        || "ERROR" | "2017-08-02 12:18:12,985"| "http-0.0.0.0-8080-4"|"AdaptorResolverUtils"
        '2017-11-17T17:36:24,179 [] [] [] [DEBUG] [EJB default - 256] ' +
                '[BaseZambasConnectionFactory] ApplicationID: TDP'                                   || "DEBUG" | '2017-11-17 17:36:24,179'| "EJB default - 256"  |"BaseZambasConnectionFactory"
        '2018-01-17T11:03:17,092 [ADC_1512996193261_1128] [1516186997043_1520] ' +
                '[NA] [DEBUG] [Thread-38 (ActiveMQ-client-global-threads-245880238)]' +
                ' [com.datalex.matrix.servants.resolution.utils.AdaptorResolverUtils] XML Object is '|| 'DEBUG' |'2018-01-17 11:03:17,092' |'Thread-38 (ActiveMQ-client-global-threads-245880238)'|'AdaptorResolverUtils'
    }
}






