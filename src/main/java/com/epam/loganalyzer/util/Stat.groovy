package com.epam.loganalyzer.util

class Stat {

    static def benchmark =
            { name,
              closure ->
//                if (name) name = closure
                long start = System.currentTimeMillis()
                def called = closure.call()
                long duration = System.currentTimeMillis() - start
                println "Execute ${name} takes ${duration}ms. : Size ${if (called instanceof Collection && !called.isEmpty()) called.size()}"
                called
            }
}
