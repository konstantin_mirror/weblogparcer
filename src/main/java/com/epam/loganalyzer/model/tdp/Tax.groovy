package com.epam.loganalyzer.model.tdp

import groovy.transform.EqualsAndHashCode


@EqualsAndHashCode
class Tax {
    String nation
    String code
    String seqNo
    String type

    String toString() {
        return "nation=" + nation +
                " code=" + code +
                " type=" + type +
                " sequenceNumber=" + seqNo
    }
}
