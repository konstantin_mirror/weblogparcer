package com.epam.loganalyzer.model.tdp

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode
@ToString
class Fare {

    String id
    String tariff
    String rule
    String clazz
    String amount
    String currency
    String fareFamily
    String routing
    String origin
    String destination
    String footnote
    String effectiveDate
    String maximumPermittedMileage
    String sequenceNumber
    String publicTariff
    String carrierCode
    String oneWayMode
    String directionIndicator

    boolean isCat25() {
        id.contains("CAT25")
    }

    boolean isConstructed() {
        id.contains("CONSTR")
    }
}
