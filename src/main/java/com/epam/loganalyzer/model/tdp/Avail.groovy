package com.epam.loganalyzer.model.tdp

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Created by Kanstantsin_Miranos on 7/11/2017.
 */
@ToString
@EqualsAndHashCode
class Avail {
    String departureAirport = ""
    String arrivalAirport = ""
    String operatingAirline = ""
    String marketingAirline = ""
    String departureDateTime = ""
    String arrivalDateTime = ""
    String flightNumber = ""
    String requiredRBD = ""
    List<BookingClassAvailability> bookingClassAvailabilities = []

    String getRequredRBDQuantity() {
        bookingClassAvailabilities.find({ it.RBD == requiredRBD }).RBDQuantity
    }
}

