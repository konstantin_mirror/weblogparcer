package com.epam.loganalyzer.model.tdp

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString


/**
 * Created by Kanstantsin_Miranos on 7/11/2017.
 */
@EqualsAndHashCode
@ToString
class BookingClassAvailability {
    String cabinCode = ""
    String cabinDescription = ""
    String RBD = ""
    String RBDQuantity = ""
}
