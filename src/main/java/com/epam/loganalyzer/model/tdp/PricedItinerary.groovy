package com.epam.loganalyzer.model.tdp

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString


@EqualsAndHashCode(excludes = ['sequenceNumber'])
@ToString
class PricedItinerary {
    String sequenceNumber = ''
    String originDestinationRPH = ''
    List<Avail> avails = []
    List<Fare> fares = []
}
