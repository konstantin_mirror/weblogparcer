package com.epam.loganalyzer.model.statistic

import com.epam.loganalyzer.model.record.LogRecord
import com.epam.loganalyzer.model.record.ServiceRecord
import com.epam.loganalyzer.model.tdp.Avail
import com.epam.loganalyzer.model.tdp.Fare
import com.epam.loganalyzer.model.tdp.PricedItinerary
import com.epam.loganalyzer.model.tdp.Tax
import org.springframework.stereotype.Component

/**
 * todo: create service, move to model
 */
@Component
class LogStatisticData {
    List<String> uniqueRows = []
    Set<Fare> removedFares = []
    Set<Tax> addedTaxes = []
    List<LogRecord> errors = []
    Map<String, List<String>> services = [:]
    List<String> fileSet = []
    List<Avail> avails = []
    List<PricedItinerary> pricedItineraries = []
    List<Fare> faresFromFS = []
    List<ServiceRecord> serviceRecords = []
    List<LogRecord> generalInfo = []

    void cleanAll() {
        uniqueRows = []
        removedFares = []
        addedTaxes = []
        services = [:]
        errors = []
        fileSet = []
        avails = []
        pricedItineraries = []
        faresFromFS = []
        serviceRecords = []
        generalInfo = []
    }
}
