package com.epam.loganalyzer.model.record

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Created by Kanstantsin_Miranos on 8/2/2017.
 */
@EqualsAndHashCode
@ToString
class LogRecord {
    String fullRecord = ''
    TimeStamp timeStamp = new TimeStamp()
    String massage = ''
}
