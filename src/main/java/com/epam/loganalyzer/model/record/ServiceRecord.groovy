package com.epam.loganalyzer.model.record

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Created by Kanstantsin_Miranos on 7/21/2017.
 */
@EqualsAndHashCode
@ToString
class ServiceRecord extends LogRecord {
    ServiceRecord() {
        clientId = ''
        uniqueId = ''
        name = ''
        containError = false
        errorMessage = []
    }

    ServiceRecord(LogRecord logRecord) {
        super()
        this.fullRecord = logRecord.fullRecord
        this.timeStamp = logRecord.timeStamp
        this.massage = logRecord.massage
    }


    String clientId
    String uniqueId
    String name
    boolean containError
    List<String> errorMessage
}
