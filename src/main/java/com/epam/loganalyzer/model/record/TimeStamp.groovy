package com.epam.loganalyzer.model.record

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Created by Kanstantsin_Miranos on 7/21/2017.
 */
@EqualsAndHashCode
@ToString
class TimeStamp {
    String thread = ''
    String time = ''
    String clazz = ''
    String type = ''
}
