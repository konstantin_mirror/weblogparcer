package com.epam.loganalyzer.service.filters.fares

import com.epam.loganalyzer.service.converter.LogUtil
import com.epam.loganalyzer.model.statistic.LogStatisticData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class RemovedFareInfo {

    @Autowired
    LogStatisticData logStatisticData

    @Autowired
    LogUtil logUtil

    List getInformation(String ID, String clazz, String tariff, String rule, int countLines = 10) {

        def removedFare = "Removing fare ${clazz}/${tariff}/${rule}/${ID}"
        def fullInfo = []
        def logFileLines = logStatisticData.uniqueRows
        for (def i = 0; i < logFileLines.size(); i++) {
            if (logFileLines[i].contains(removedFare)) {
                List infoAboutRemovedFare = []
                infoAboutRemovedFare << logFileLines[i]
                String currentTread = logUtil.selectElements(logFileLines[i]).timeStamp.thread
                def listForInfo = logFileLines.subList(0, i).reverse()
                for (record in listForInfo) {
                    if (!record.contains("Removing fare") && record.contains(currentTread)) {
                        infoAboutRemovedFare << record
                        if (infoAboutRemovedFare.size() > countLines) {
                            break
                        }
                    }
                }
                fullInfo << infoAboutRemovedFare.reverse()
            }
        }
        return fullInfo
    }
}
