package com.epam.loganalyzer.service.filters.fares

import com.epam.loganalyzer.model.tdp.Fare

/**
 * Created by Kanstantsin_Miranos on 8/10/2017.
 * todo: move to service-filters
 */
class FaresUtil {

    // todo: refactoring in parameters
    static List<Fare> filteringFares(List<Fare> fares, String clazz,
                                     String tariff, String rule, String id) {

        List<Fare> filteredFares = fares
        if (!clazz.isEmpty()) {
            filteredFares = filteredFares.findAll { it.clazz.toLowerCase().contains(clazz.toLowerCase()) }
        }
        if (!tariff.isEmpty()) {
            filteredFares = filteredFares.findAll { it.tariff.toLowerCase().contains(tariff.toLowerCase()) }
        }
        if (!rule.isEmpty()) {
            filteredFares = filteredFares.findAll { it.rule.toLowerCase().contains(rule.toLowerCase()) }
        }
        if (!id.isEmpty()) {
            filteredFares = filteredFares.findAll { it.id.toLowerCase().contains(id.toLowerCase()) }
        }
        return filteredFares
    }
}
