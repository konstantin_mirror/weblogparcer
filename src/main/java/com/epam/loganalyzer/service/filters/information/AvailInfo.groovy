package com.epam.loganalyzer.service.filters.information

import com.epam.loganalyzer.model.tdp.Avail
import com.epam.loganalyzer.model.tdp.BookingClassAvailability
import org.springframework.stereotype.Service

/**
 * Created by Kanstantsin_Miranos on 7/11/2017.
 */

@Service
class AvailInfo {

    List<Avail> availInfo(List<String> service) {
        println("Parsing -> DLX_AirAvailSvRS")
        List<Avail> avails = []
        def parser = new XmlSlurper()
        service.each { RS ->
            RS = RS.replaceAll("\n", "")
            RS = (RS =~ ".*(<DLX_AirAvailSvRS.*>)")[0][1]
            def avail = parser.parseText(RS)
            avail.OTA_AirAvailRS.OriginDestinationOptions.OriginDestinationOption.each {
                def segment = it.FlightSegment
                if (segment.size() == 1) {
                    avails << getSegmentAvailInfo(segment)
                } else {
                    List<Avail> availsForOneSegment = []
                    segment.each { availsForOneSegment << getSegmentAvailInfo(it) }
                    avails << mergeAvails(availsForOneSegment)
                }
            }
        }
        return avails.unique()
    }

    static Avail mergeAvails(List<Avail> avails) {
        Avail avail = new Avail()
        avails.each {
            avail.departureAirport += "${it.departureAirport}     "
            avail.arrivalAirport += "${it.arrivalAirport}     "
            avail.requiredRBD += "${it.requiredRBD}     "
            avail.departureDateTime += "${it.departureDateTime}     "
            avail.arrivalDateTime += "${it.arrivalDateTime}     "
            avail.operatingAirline += "${it.operatingAirline}     "
            avail.marketingAirline += "${it.marketingAirline}     "
            avail.flightNumber += "${it.flightNumber}     "
            it.bookingClassAvailabilities.each { current ->
                avail.bookingClassAvailabilities.add(current)
            }
            avail.bookingClassAvailabilities.
                    add(new BookingClassAvailability([RBD: "---", RBDQuantity: "---", cabinDescription: "---", cabinCode: "---"]))


        }
        return avail
    }

    static Avail getSegmentAvailInfo(def segment) {
        Avail availUnit = new Avail()
        availUnit.arrivalDateTime = segment.@ArrivalDateTime
        availUnit.departureDateTime = segment.@DepartureDateTime
        availUnit.departureAirport = segment.DepartureAirport.@LocationCode
        availUnit.arrivalAirport = segment.ArrivalAirport.@LocationCode
        availUnit.flightNumber = segment.@FlightNumber
        availUnit.marketingAirline = segment.MarketingAirline.@Code
        availUnit.operatingAirline = segment.OperatingAirline.@Code
        availUnit.requiredRBD = segment.@ResBookDesigCode
        segment.TPA_Extension.BookingClassAvailability.each {
            BookingClassAvailability availability = new BookingClassAvailability()
            availability.cabinCode = it.@CabinCode
            availability.cabinDescription = it.@CabinDescription
            availability.RBD = it.@ResBookDesigCode
            availability.RBDQuantity = it.@ResBookDesigQuantity
            if (availability.RBDQuantity == null || availability.RBDQuantity == '') {
                availability.RBDQuantity = it.@ResBookDesigStatusCode
            }
            availUnit.bookingClassAvailabilities << availability
        }
        return availUnit
    }
}
