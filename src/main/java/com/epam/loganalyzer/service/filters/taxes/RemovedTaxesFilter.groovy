package com.epam.loganalyzer.service.filters.taxes

import org.springframework.stereotype.Service

import java.util.regex.Matcher
import java.util.regex.Pattern

@Service
class RemovedTaxesFilter {

    final String PATTERN_TEMPLATE = "(Processing .+ TaxRules taxableUnitType=.+ nation=.+ code=.+ type=.+ taxPointType=.+ percentFlatType=.+)||" +
            "(Processing TaxRule sequenceNumber=.+)||" +
            "(rejected TaxRule sequenceNumber=.+) ||" +
            "(Skipping TaxRule sequenceNumber=.+: Exempt=true)||" +
            "(Unsupported Alternate Rule Reference for TaxRule sequenceNumber=.+) ||" +
            "(No match for TaxRule nation=.+ code=.+ type=.+)"

    Pattern PATTERN = Pattern.compile(PATTERN_TEMPLATE)

    private String selectSequenceLine

    RemovedTaxesFilter() {
        selectSequenceLine = "com.epam.tdp.tax.atpco.ATPCOTaxQuotationServiceImpl"
    }

    RemovedTaxesFilter(String selectSequenceLine) {
        this.selectSequenceLine = selectSequenceLine
    }


    List<String> parser(List<String> lines) {
        List<String> applicableTaxes = new ArrayList<>()

        lines.each { String line ->
            Matcher matcher = PATTERN.matcher(line)
            if (matcher.find()) {
                applicableTaxes << line
            }
        }
        applicableTaxes
    }
}
