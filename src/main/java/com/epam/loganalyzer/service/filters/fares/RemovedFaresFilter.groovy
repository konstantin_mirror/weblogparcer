package com.epam.loganalyzer.service.filters.fares

import com.epam.loganalyzer.model.tdp.Fare
import org.springframework.stereotype.Service

import java.util.regex.Matcher
import java.util.regex.Pattern
import java.util.stream.Collectors

@Service
class RemovedFaresFilter {

    final String PATTERN_TEMPLATE = "Removing fare (.+)/(.+)/(.+)/(.+)"
    final Pattern PATTERN = Pattern.compile(PATTERN_TEMPLATE)

    Set<Fare> parser(List<String> lines) {
        println("Searching removed fares in log")
        lines.parallelStream().filter { it.contains('Removing fare') }.
                map {
                    Fare fare = null
                    Matcher matcher = PATTERN.matcher(it)
                    if (matcher.find()) {
                        fare = new Fare()
                        fare.setClazz(matcher.group(1))
                        fare.setTariff(matcher.group(2))
                        fare.setRule(matcher.group(3))
                        fare.setId(matcher.group(4))
                    }
                    return fare
                }.filter { it != null }.collect(Collectors.toSet())
    }
}
