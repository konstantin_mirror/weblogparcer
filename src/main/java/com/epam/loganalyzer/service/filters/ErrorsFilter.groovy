package com.epam.loganalyzer.service.filters

import com.epam.loganalyzer.service.converter.LogUtil
import com.epam.loganalyzer.model.record.LogRecord
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import java.util.stream.Collectors

/**
 * Created by Kanstantsin_Miranos on 6/16/2017.
 */
@Service
class ErrorsFilter {

    @Autowired
    LogUtil logUtil

    List<LogRecord> lineToLogRecord(List<String> logLines) {
        long start = System.currentTimeMillis();
        def records = lineToLogRecord(logLines, "[ERROR]")
        println "duration lineToLogRecord: " + (System.currentTimeMillis() - start)
        records
    }

    List<LogRecord> lineToLogRecord(List<String> logLines, String tag) {
        println("Searching ${tag} in log")
        List<LogRecord> errorList = logLines.parallelStream().filter({
            it.contains(tag)
        }).map {
            LogRecord logRecord = logUtil.selectElements(it)
            List<String> bodySplit = logRecord.fullRecord.split("\n")
            String shortMassage = bodySplit[0]
            shortMassage << (shortMassage.size() > 200 ? shortMassage[0..200] : shortMassage)
            if (shortMassage ==~ /\s*/) {
                if (bodySplit.size() > 2) {
                    shortMassage = bodySplit[1]
                } else {
                    shortMassage = '****'
                }
            }
            logRecord.massage = shortMassage
            return logRecord
        }.collect(Collectors.toList())

        return errorList
    }
}
