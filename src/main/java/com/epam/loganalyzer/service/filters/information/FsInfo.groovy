package com.epam.loganalyzer.service.filters.information

import com.epam.loganalyzer.model.tdp.Fare
import org.springframework.stereotype.Service

/**
 * Created by Kanstantsin_Miranos on 7/14/2017.
 */
@Service
class FsInfo {

    List<Fare> getFaresSearchRsInfo(List<String> service) {
        println("Parsing -> FareSearchSvRS")
        List<Fare> fares = []
        def parser = new XmlSlurper()
        service?.each { RS ->
            RS = RS.replaceAll("\n", "")
            RS = (RS =~ ".*(<ns2:FareSearchSvRS.*>)")[0][1]
            def fsRs = parser.parseText(RS)
            fsRs.FaresAndRules.Fares.Fare.each {
                Fare fare = new Fare()
                fare.carrierCode = it.@CarrierCode
                fare.clazz = it.@FareClass
                fare.rule = it.@RuleNumber
                fare.tariff = it.@TariffNumber
                fare.id = it.@ID
                fare.fareFamily = it.@FareFamilies
                fare.amount = it.@OriginalAmount
                fare.currency = it.@CurrencyCode
                fare.origin = it.@Origin
                fare.destination = it.@Destination
                fare.footnote = it.@Footnote
                fare.routing = it.@RoutingNumber
                fare.publicTariff = it.@PublicTariff
                fare.effectiveDate = it.@EffectiveDate
                fare.maximumPermittedMileage = it.@MaximumPermittedMileage
                fare.sequenceNumber = it.@SequenceNumber
                fare.directionIndicator = it.@DirectionIndicator
                fare.oneWayMode = it.@OneWayMode
                fares << fare
            }
        }
        return fares.unique()
    }
}

