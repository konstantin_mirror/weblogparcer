package com.epam.loganalyzer.service.filters

import com.epam.loganalyzer.service.converter.LogUtil
import com.epam.loganalyzer.model.record.LogRecord
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

import java.util.stream.Collectors

/**
 * Created by Kanstantsin_Miranos on 8/1/2017.
 */
@ConfigurationProperties(prefix = "list.of")
@Component
class GeneralInfo {

    List<String> info

    @Autowired
    LogUtil logUtil

    List<LogRecord> getGeneralInfo(List<String> logLines) {
        println("Searching general info")
        List<LogRecord> generalInfoLines = logLines.parallelStream().filter { logLine ->
            info.any { info ->
                logLine.contains(info)
            }
        } map { infoLine ->
            LogRecord logRecord = logUtil.selectElements(infoLine)
            logRecord.massage = logRecord.fullRecord.size() > 300 ?
                    logRecord.fullRecord[0..300] : logRecord.fullRecord

            return logRecord
        } collect(Collectors.toList())
        return generalInfoLines
    }
}
