package com.epam.loganalyzer.service.filters.taxes

import com.epam.loganalyzer.model.tdp.Tax
import org.springframework.stereotype.Service

import java.util.regex.Matcher
import java.util.regex.Pattern
import java.util.stream.Collectors

@Service
class AddedTaxesFilter {

    final String PATTERN_TEMPLATE = "Adding TaxRule nation=(.+) code=(.+) type=(.+) sequenceNumber=(.+)"
    final Pattern PATTERN = Pattern.compile(PATTERN_TEMPLATE)

    Set<Tax> parser(List<String> lines) {
        println "searching taxes"
        Set<Tax> taxes = lines.parallelStream().filter { it.contains('Adding TaxRule nation=') }.
                map {
                    Tax taxe = null
                    Matcher matcher = PATTERN.matcher(it)
                    if (matcher.find()) {
                        taxe = new Tax();
                        taxe.setNation(matcher.group(1))
                        taxe.setCode(matcher.group(2))
                        taxe.setType(matcher.group(3))
                        taxe.setSeqNo(matcher.group(4))
                    }
                    return taxe
                }.filter { it != null }.
                collect(Collectors.toSet())
        return taxes
    }
}
