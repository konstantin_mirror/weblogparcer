package com.epam.loganalyzer.service.filters.information

import com.epam.loganalyzer.model.tdp.Fare
import com.epam.loganalyzer.model.tdp.PricedItinerary
import org.springframework.stereotype.Service

/**
 * Created by Kanstantsin_Miranos on 7/12/2017.
 */
@Service
class AlfsRsInfo {

    List<PricedItinerary> pricedItinerary(List<String> service) {
        println("Parsing -> DLX_AirLowFareSearchSvRS")
        List<PricedItinerary> itinerarys = []
        def parser = new XmlSlurper()
        service?.each { RS ->
            RS = RS.replaceAll("\n", "")
            RS = (RS =~ ".*(<DLX_AirLowFareSearchSvRS.*>)")[0][1]
            def LFS_RS = parser.parseText(RS)
            LFS_RS.OTA_AirLowFareSearchRS.PricedItineraries.PricedItinerary.each { pricedItinerary ->
                PricedItinerary currentpricedItinerary = new PricedItinerary()
                currentpricedItinerary.sequenceNumber = pricedItinerary.@SequenceNumber
                currentpricedItinerary.originDestinationRPH = pricedItinerary.@OriginDestinationRPH
                pricedItinerary.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.each {
                    currentpricedItinerary.avails << AvailInfo.getSegmentAvailInfo(it)
                }
                pricedItinerary.AirItineraryPricingInfo.FareInfos.FareInfo.each {
                    Fare fare = new Fare()
                    fare.clazz = it.FareReference.text()
                    fare.id = getID(it.@NameValues as String)
                    fare.rule = it.TPA_Extension.ATPCOFareData.@RuleNumber
                    fare.tariff = it.TPA_Extension.ATPCOFareData.@Tariff
                    fare.routing = it.TPA_Extension.ATPCOFareData.@RoutingNumber
                    fare.amount = it.TPA_Extension.BaseFare.@Amount
                    fare.currency = it.TPA_Extension.BaseFare.@CurrencyCode
                    fare.fareFamily = it.TPA_Extension.FareFamilySummaryReference.@FareFamilyCode
                    fare.origin = it.DepartureAirport.@LocationCode
                    fare.destination = it.ArrivalAirport.@LocationCode
                    currentpricedItinerary.fares << fare
                }
                itinerarys << currentpricedItinerary
            }
        }
        return itinerarys.unique()
    }

    String getID(String id) {
        if (id.contains('FareID')) {
            return (id =~ ".*FareID=(.*)~TDPFARES.FareType.*")[0][1]
        } else
            return ""
    }
}
