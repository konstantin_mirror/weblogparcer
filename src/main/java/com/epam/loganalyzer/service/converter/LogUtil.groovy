package com.epam.loganalyzer.service.converter

import com.epam.loganalyzer.model.record.LogRecord
import com.epam.loganalyzer.model.record.TimeStamp
import org.springframework.stereotype.Component

import java.text.SimpleDateFormat
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Created by Kanstantsin_Miranos on 8/2/2017.
 */
@Component
class LogUtil {


    final Pattern PATTERN = ~/(?ms)^(.{1,24})(\[.{0,30}\]\s\[.{0,50}\]\s\[.{0,2}\]\s){0,1}\[(.{0,10})\]\s\[(.{0,100})\]\s\[([\w\d._]+)\](.*)/

    LogRecord selectElements(String record) {
        TimeStamp timeStamp = new TimeStamp()
        LogRecord logRecord = new LogRecord()
        try {
            Matcher matcher = PATTERN.matcher(record)
            if (matcher.matches()) {

                timeStamp.time = matcher.group(1).replaceFirst("T", " ").trim()
                timeStamp.type = matcher.group(3).trim()
                timeStamp.thread = matcher.group(4).trim()
                timeStamp.clazz = matcher.group(5).split(/\./)[-1].trim()
                logRecord.fullRecord = matcher.group(6).trim()
                logRecord.timeStamp = timeStamp
            }

        }
        catch (Exception e) {
            println(e)
            println(record)
            throw new IllegalStateException()
        }
        return logRecord
    }

    Date parseDate(String date) {
        Date logRecordDate
        try {
            logRecordDate = Date.parse("yyyy-MM-dd HH:mm:ss,SSS", date.trim())
        }
        catch (Exception e) {
            println("WARNING unpaseble date --> $date")
            logRecordDate  = new Date(0)
        }
        return logRecordDate
    }

}
