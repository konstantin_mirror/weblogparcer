package com.epam.loganalyzer.service

import com.epam.loganalyzer.service.filters.ErrorsFilter
import com.epam.loganalyzer.service.filters.GeneralInfo
import com.epam.loganalyzer.service.filters.fares.RemovedFaresFilter
import com.epam.loganalyzer.service.filters.information.AlfsRsInfo
import com.epam.loganalyzer.service.filters.information.AvailInfo
import com.epam.loganalyzer.service.filters.information.FsInfo
import com.epam.loganalyzer.service.filters.taxes.AddedTaxesFilter
import com.epam.loganalyzer.model.statistic.LogStatisticData
import com.epam.loganalyzer.util.Stat
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.stream.Collectors
import java.util.zip.GZIPInputStream
import java.util.zip.ZipFile

@Service
class LogParser {

    @Autowired
    RemovedFaresFilter removedFaresFilter
    @Autowired
    AddedTaxesFilter addedTaxesFilter
    @Autowired
    FilesCreator filesCreator
    @Autowired
    LogStatisticData logStatisticData
    @Autowired
    ErrorsFilter errorsFilter
    @Autowired
    AvailInfo availInfo
    @Autowired
    AlfsRsInfo alfsRsInfo
    @Autowired
    FsInfo fareSearchInfo
    @Autowired
    FullServiceInfo fullServiceInfo
    @Autowired
    GeneralInfo generalInfo

    LogStatisticData parseLogFile(Path uploadedFile) throws Exception {
        logStatisticData.cleanAll()
        println "upload file is : ${uploadedFile}"

        List<String> logFileLines = Stat.benchmark('getLinesWithTimestamp', { getLinesWithTimestamp(uploadedFile) })

        Stat.benchmark('toFile', { toFile(Paths.get("upload-dir/FullCleanLog.log"), logFileLines) })

        logStatisticData.uniqueRows = logFileLines

        logStatisticData.setErrors(errorsFilter.lineToLogRecord(logFileLines))

        filesCreator.create(logFileLines)
        logStatisticData.setRemovedFares(removedFaresFilter.parser(logFileLines))
        logStatisticData.setAddedTaxes(addedTaxesFilter.parser(logFileLines))
        logStatisticData.setAvails(availInfo.
                availInfo(logStatisticData.services.get('DLX_AirAvailSvRS')).unique())
        logStatisticData.setPricedItineraries(alfsRsInfo.
                pricedItinerary(logStatisticData.services.get('DLX_AirLowFareSearchSvRS')))
        logStatisticData.setFaresFromFS(fareSearchInfo.
                getFaresSearchRsInfo(logStatisticData.services.get('ns2:FareSearchSvRS')))
        logStatisticData.setServiceRecords(fullServiceInfo.getInfo(logStatisticData.services))
        logStatisticData.generalInfo = generalInfo.getGeneralInfo(logFileLines)
        return logStatisticData
    }


    static List<String> getLinesWithTimestamp(Path path) {

        BufferedReader reader = getReader(path)

        List<List<String>> lines = []
        List<String> line = []
        reader.lines().each { currentLine ->
            if (currentLine.contains("[DEBUG]") ||
                    currentLine.contains("[ERROR]") ||
                    currentLine.contains("[INFO ") ||
                    currentLine.contains("[WARN ")
            ) {
                lines.add(line)
                line = []
                line << currentLine
            } else {
                line << currentLine
            }
        }
        reader.close()
        lines.parallelStream().map { return it.join('\n') }.collect(Collectors.toList())
    }

    private static BufferedReader getReader(Path path) {
        def fileName = path.toFile().getName()
        def ext = fileName[fileName.lastIndexOf('.')+1..-1]
        if (ext == 'gz'){
            InputStream fileStream = new FileInputStream(path.toFile());
            InputStream gzipStream = new GZIPInputStream(fileStream);
            Reader decoder = new InputStreamReader(gzipStream);
            new BufferedReader(decoder);
        }else{
            Files.newBufferedReader(path)
        }

    }


    static void toFile(Path path, List<String> source) {
        BufferedWriter writer = Files.newBufferedWriter(path)
        source.each {
            writer.write(it)
            writer.newLine()
        }
        writer.flush()
        writer.close()
    }
}

