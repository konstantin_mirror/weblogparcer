package com.epam.loganalyzer.service

import com.epam.loganalyzer.service.converter.LogUtil
import com.epam.loganalyzer.model.record.ServiceRecord
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import java.util.regex.Pattern

/**
 * Created by Kanstantsin_Miranos on 7/21/2017.
 */
@Component
class FullServiceInfo {

    private static final Pattern CLIENT_ID_PATTERN = ~/\sClientId="([-_\w]+)"\s/
    private static final Pattern UNIQUE_ID_PATTERN = ~/\sUniqueId="([-:_\w.]+)"\s/

    @Autowired
    LogUtil logUtil

    List<ServiceRecord> getInfo(Map<String, List<String>> services) {
        println('Parsing -> services records')
        List<ServiceRecord> serviceRecords = []
        services.each { serviceName, records ->
            records.each { record ->
                String firstLine = record.split('\n')[0]
                ServiceRecord serviceRecord = getServiceRecord(firstLine)
                serviceRecord.fullRecord = record
                serviceRecord.name = serviceName
                if (record.contains("ClientId")) {
                    serviceRecord.clientId = (record =~ CLIENT_ID_PATTERN)[0][1]
                }
                if (record.contains("UniqueId")) {

                    String uniqueId = (record =~ UNIQUE_ID_PATTERN)[0][1]
                    if (!uniqueId.empty && uniqueId.size() > 7) {

                        serviceRecord.uniqueId = uniqueId[-5..-1]
                    }
                }
                if (record.contains("WarningCode")) {
                    serviceRecord.containError = true
                }
                serviceRecords << serviceRecord
            }
        }
        println "serviceRecords.size(): ${serviceRecords.size()}"

        return serviceRecords
    }

    ServiceRecord getServiceRecord(String line) {
        ServiceRecord serviceRecord = new ServiceRecord(logUtil.selectElements(line))
        String shortMassage
        if (serviceRecord.fullRecord.contains('<')) {
            shortMassage = (serviceRecord.fullRecord =~ /([^<]*)(.*)/)[0][1]
        } else {
            shortMassage = serviceRecord.fullRecord.split("\n")[0]
        }
        shortMassage << (shortMassage.size() > 200 ? shortMassage[0..200] : shortMassage)
        serviceRecord.massage = shortMassage
        return serviceRecord
    }
}
