package com.epam.loganalyzer.service

import com.epam.loganalyzer.model.statistic.LogStatisticData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Service

import java.nio.file.Paths
import java.util.stream.Collectors

@ConfigurationProperties(prefix = "list.of")
@Service
class FilesCreator {

    @Autowired
    LogStatisticData logStatisticData

    List<String> services

    void create(List<String> lines) {
        services.each { name->
            println("working in ${name}.xml")
            def xmlService = lines.parallelStream().filter({ it.contains("<${name}") }).collect(Collectors.toList())
            if (xmlService) {
                logStatisticData.services << [(name): xmlService]
                def file = Paths.get("upload-dir/${name.replace(':', '_')}.xml").toFile()
                file << xmlService
                file << '\n'
                file << '\n'
            }
        }
    }
}



