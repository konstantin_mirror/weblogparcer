package com.epam.loganalyzer;

import com.epam.loganalyzer.storage.Storage;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner init(Storage storage) {
        return (args) -> {
            storage.deleteAll();
            storage.init();
        };
    }
}
