package com.epam.loganalyzer.controller

import com.epam.loganalyzer.model.statistic.LogStatisticData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.ModelAndView

/**
 * Created by Kanstantsin_Miranos on 5/26/2017.
 */
@Controller
@RequestMapping("/taxes")
class TaxController {


    @Autowired
    LogStatisticData logStatisticData

// todo: rest naming conventions
    @RequestMapping("/applicableTaxes")
    def applicableTaxes() {
        // todo: rest naming conventions
        new ModelAndView("views/taxesAndFees", [taxes: logStatisticData.getAddedTaxes(),
                                                files: logStatisticData.fileSet])
    }
}
