package com.epam.loganalyzer.controller

import com.epam.loganalyzer.model.record.LogRecord
import com.epam.loganalyzer.model.statistic.LogStatisticData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView

/**
 * Created by Kanstantsin_Miranos on 6/16/2017.
 */

@Controller
@RequestMapping('/errors')
class ErrorsInformationController {

    @Autowired
    LogStatisticData logStatisticData

    // todo: rest naming conventions
    @RequestMapping("/allErrors")
    // todo: naming conventions
    def getAllerrors() {
        new ModelAndView("views/errors", [files: logStatisticData.fileSet, "errorsList": logStatisticData.errors])
    }

    @RequestMapping("/error")
    def getError(@RequestParam String time, @RequestParam String thread,
                 @RequestParam String type, @RequestParam String clazz) {
        List<String> stackTrace = []

        // todo: use service for logic
        logStatisticData.getErrors().each {
            if (it.timeStamp.time == time && it.timeStamp.thread == thread &&
                    it.timeStamp.type == type && it.timeStamp.clazz == clazz) {
                stackTrace = it.fullRecord.split("\n").toList()
            }
        }
        // todo: rest naming conventions
        new ModelAndView("views/errorInfo", [files       : logStatisticData.fileSet,
                                             "errorsList": stackTrace])
    }

    // todo: rest naming conventions
    @RequestMapping("/specificErrors")
    def getSpecificErrors(
            @RequestParam(required = false) String massage, @RequestParam(required = false) String clazz) {

        // todo: use service for logic
        List<LogRecord> er = []
        if (massage) {
            er.addAll(logStatisticData.errors.findAll { it.massage.contains(massage - '....') })
        }
        if (clazz) {
            er.addAll(logStatisticData.errors.findAll { it.timeStamp.clazz.contains(clazz) })
        }
        new ModelAndView("views/errors", [files       : logStatisticData.fileSet,
                                          "errorsList": er])
    }

    // todo: rest naming conventions
    @RequestMapping("/Statistic")
    def getErrorStatistic() {
        // todo: use service for logic
        Map<String, Integer> massages = new LinkedHashMap<>()
        Map<String, Integer> classes = new LinkedHashMap<>()

        def fun = { Map<String, Integer> statistic, String unit ->
            if (statistic.keySet().contains(unit)) {
                statistic.put(unit, statistic.get(unit) + 1)
            } else {
                statistic.put(unit, 1)
            }
        }

        logStatisticData.getErrors().each {
            fun(massages, it.massage)
            fun(classes, it.timeStamp.clazz)
        }

        massages = massages.sort { -it.value }
        Map<String, Integer> massages_2 = new LinkedHashMap<>()
        massages.keySet().each {
            if (it.size() > 100) {
                massages_2.put(it[0..100] + '....', massages.get(it))
            } else {
                massages_2.put(it, massages.get(it))
            }

        }
        classes = classes.sort { -it.value }
        // todo: rest naming conventions
        new ModelAndView("views/errorsStatistic", [files              : logStatisticData.fileSet,
                                                   "massagesStatistic": massages_2,
                                                   "classesStatistic" : classes])
    }

}
