package com.epam.loganalyzer.controller

import com.epam.loganalyzer.model.tdp.Fare
import com.epam.loganalyzer.service.filters.fares.FaresUtil
import com.epam.loganalyzer.service.filters.fares.RemovedFareInfo
import com.epam.loganalyzer.model.statistic.LogStatisticData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView

@Controller
@RequestMapping("/fares")
class FaresConroller {


    @Autowired
    RemovedFareInfo removedFareInfo
    @Autowired
    LogStatisticData logStatisticData


    @GetMapping("/removed")
    def removedFares() {
        new ModelAndView("views/removedFares", [files  : logStatisticData.fileSet,
                                                "fares": logStatisticData.getRemovedFares().sort { it.clazz }])
    }

    @PostMapping("/removed")
    def removedFares(@RequestParam(required = false) String clazz,
                     @RequestParam(required = false) String tariff,
                     @RequestParam(required = false) String rule,
                     @RequestParam(required = false) String id) {
// todo: use service for logic
        List<Fare> fares = FaresUtil.filteringFares(logStatisticData.removedFares.toList(), clazz.trim(),
                tariff.trim(), rule.trim(), id.trim())

        new ModelAndView("views/removedFares", [files : logStatisticData.fileSet.toList(),
                                                fares : fares.sort { it.clazz }, clazz: "${clazz}",
                                                tariff: "${tariff}", rule: "${rule}", id: "${id}"])
    }

    @RequestMapping("/fare")
    // todo: naming conventions
    def getfare(
            @RequestParam String clazz,
            @RequestParam String tariff, @RequestParam String rule, @RequestParam String ID) {

        removedFareInfo.getInformation(ID, clazz, tariff, rule)
        new ModelAndView(
                "views/fareInfo",
                [info : removedFareInfo.getInformation(ID, clazz, tariff, rule),
                 files: logStatisticData.fileSet])
    }

// todo: rest naming conventions
    @RequestMapping("/faresQuoted")
    def faresquoted() {
        // todo: use service for logic
        List<Fare> faresQuoted = []
        logStatisticData.pricedItineraries.each {
            faresQuoted.addAll(it.fares)
        }
        faresQuoted.unique().sort { it.clazz }
        new ModelAndView("views/faresQuoted", [files  : logStatisticData.fileSet,
                                               "fares": faresQuoted])
    }

    // todo: rest naming conventions
    @GetMapping("/fromFS")
    // todo: naming conventions
    def faresfromFS() {
        new ModelAndView("views/FareSearchRS", [files  : logStatisticData.fileSet,
                                                "fares": logStatisticData.getFaresFromFS().sort { it.clazz }])
    }

    // todo: rest naming conventions
    @PostMapping("/fromFS")
    def faresfromFS(@RequestParam(required = false) String clazz,
                    @RequestParam(required = false) String tariff,
                    @RequestParam(required = false) String rule,
                    @RequestParam(required = false) String id) {
        // todo: use service for logic
        List<Fare> fares = FaresUtil.filteringFares(logStatisticData.faresFromFS.toList(), clazz.trim(),
                tariff.trim(), rule.trim(), id.trim())
        // todo: rest naming conventions
        new ModelAndView("views/FareSearchRS", [files: logStatisticData.fileSet,
                                                fares: fares.sort { it.clazz },
                                                clazz: "${clazz}", tariff: "${tariff}",
                                                rule : "${rule}", id: "${id}"])
    }


}
