package com.epam.loganalyzer.controller

import com.epam.loganalyzer.model.record.LogRecord
import com.epam.loganalyzer.model.statistic.LogStatisticData
import com.epam.loganalyzer.service.converter.LogUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView

/**
 * Created by Kanstantsin_Miranos on 7/21/2017.
 */
@Controller
@RequestMapping("/service")
class ServiseRecordsController {

    @Autowired
    LogStatisticData logStatisticData

    @Autowired
    LogUtil logUtil

    // todo: rest naming conventions
    @RequestMapping("/getAll")
    def getRecords(@RequestParam(required = false) boolean info,
                   @RequestParam(required = false) boolean error) {
        // todo: use service for logic
        List<LogRecord> logRecords = []
        logRecords.addAll(logStatisticData.serviceRecords)
        if (info) {
            logRecords.addAll(logStatisticData.generalInfo)
        }
        if (error) {
            logRecords.addAll(logStatisticData.errors)
        }
        logRecords.sort {
            logUtil.parseDate(it.timeStamp.time)
        }
        new ModelAndView("views/serviceRecords", [records: logRecords,
                                                  files  : logStatisticData.fileSet])
    }

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        // todo: use service for logic
        List<String> name = filename.split('!')
        String service = logStatisticData.serviceRecords.findAll({
            it.timeStamp.time == name[1] &&
                    it.fullRecord.contains(name[0])
        }).fullRecord
        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + name[0] + ".xml" + "\"")
                .body(service)
    }

    // todo: rest naming conventions
    @PostMapping("/files/collectedServices")
    @ResponseBody
    ResponseEntity<Resource> collectedServices(
            @RequestParam(required = false, value = "servicesNames") List<String> servicesNames) {
        // todo: use service for logic
        StringBuilder builder = new StringBuilder()
        servicesNames.each { service ->
            List<String> name = service.split('!')
            builder.append(logStatisticData.serviceRecords.findAll({
                it.timeStamp.time == name[1] &&
                        it.fullRecord.contains(name[0])
            }).fullRecord)
            builder.append('\n')
            builder.append('\n')
        }
        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + "Gathered Files.xml" + "\"")
                .body(builder)
    }
}
