package com.epam.loganalyzer.controller

import com.epam.loganalyzer.service.LogParser
import com.epam.loganalyzer.model.statistic.LogStatisticData
import com.epam.loganalyzer.storage.StorageFileNotFoundException
import com.epam.loganalyzer.storage.Storage
import com.epam.loganalyzer.util.Stat
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.mvc.support.RedirectAttributes

@Controller
class FileUploadController {

    @Autowired
    private Storage storage

    @Autowired
    private LogParser logParser

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    ResponseEntity<Resource> serveFile(@PathVariable String filename) throws Exception {
        Resource file = storage.loadAsResource(filename)
        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file)
    }

    // todo: rest naming conventions
    @GetMapping("/project/cleanAll")
    String cleanAll() {
        storage.deleteAll()
        storage.init()
        return "redirect:/home"
    }


    @PostMapping("/files")
    String handleFileUpload(@RequestParam("file") MultipartFile file, RedirectAttributes attr) {
        Stat.benchmark('handleFileUpload', {
            storage.deleteAll()
            storage.init()
            storage.store(file)

            LogStatisticData logData = Stat.benchmark('parseLogFile', {
                logParser.parseLogFile(storage.load(file.getOriginalFilename()))
            })

            storage.loadAll().each() { logData.fileSet << it.toString() }

            attr.addFlashAttribute("message",
                    "You successfully uploaded " + file.getOriginalFilename() + "!")

            storage.load(file.getOriginalFilename())
        })
        return "redirect:/home"
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    ResponseEntity handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build()
    }

    // todo: need to rework handling mechanism
    @ExceptionHandler(Throwable.class)
    ResponseEntity handleThrowable(Throwable throwable) {
        ResponseEntity.status(HttpStatus.FORBIDDEN).body(throwable.getMessage() + throwable.getStackTrace());

    }

}
