package com.epam.loganalyzer.controller

import com.epam.loganalyzer.model.statistic.LogStatisticData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.ModelAndView

/**
 * Created by Kanstantsin_Miranos on 6/8/2017.
 */
@Controller
class HomeController {

    @Autowired
    LogStatisticData logStatisticData


    @RequestMapping("/home")
    def home(Model model) {
        new ModelAndView("views/home", ["logData": logStatisticData,
                                        "message": model.asMap().get("message")])
    }


}
