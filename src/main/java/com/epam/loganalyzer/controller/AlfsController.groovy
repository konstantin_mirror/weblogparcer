package com.epam.loganalyzer.controller

import com.epam.loganalyzer.model.statistic.LogStatisticData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.ModelAndView

/**
 * Created by Kanstantsin_Miranos on 7/13/2017.
 */
@Controller
@RequestMapping('/alfs')
class AlfsController {

    @Autowired
    LogStatisticData logStatisticData

    // todo: rest naming conventions
    @RequestMapping("/RS")
    def getAllAvails() {
        // todo: rest naming conventions
        new ModelAndView("views/alfsRS", [files        : logStatisticData.fileSet,
                                          "itineraries": logStatisticData.pricedItineraries])
    }
}
