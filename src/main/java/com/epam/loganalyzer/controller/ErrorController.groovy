package com.epam.loganalyzer.controller

import com.epam.loganalyzer.model.statistic.LogStatisticData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.ModelAndView
import javax.servlet.http.HttpServletRequest


@ControllerAdvice
class ExceptionHandlerController {

    @Autowired
    LogStatisticData logStatisticData


    @ExceptionHandler(value = [Exception.class, RuntimeException.class])
    def defaultErrorHandler(HttpServletRequest request, Exception e) {
        new ModelAndView("views/exceptionHandler", [files    : logStatisticData.fileSet,
                                                    exception: e,
                                                    url      : request.getRequestURL()])
    }
}