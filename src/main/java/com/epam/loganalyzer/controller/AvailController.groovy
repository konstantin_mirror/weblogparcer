package com.epam.loganalyzer.controller

import com.epam.loganalyzer.model.statistic.LogStatisticData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.ModelAndView

/**
 * Created by Kanstantsin_Miranos on 7/11/2017.
 */
@Controller
@RequestMapping('/avail')
class AvailController {


    @Autowired
    LogStatisticData logStatisticData

    // todo: rest naming conventions
    @RequestMapping("/allAvails")
    def getAllAvails() {
        new ModelAndView("views/avail", [files   : logStatisticData.fileSet,
                                         "avails": logStatisticData.avails])
    }

}
