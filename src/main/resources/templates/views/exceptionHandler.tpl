package templates.views

layout 'layouts/main.tpl',
        filesList: files,
        mainBody: contents {
            div(class: 'container') {
                Exception ex = exception

                div(class: 'alert alert-danger') {
                    strong("Sorry  some problems occurred during processing...")
                }
                button(type: 'button', class: 'btn btn-info', 'data-toggle': 'collapse',
                        'data-target': '#info', 'Show more..')
                div(id: 'info', class: 'collapse') {
                    p (class:'text-primary'," CAUSE: ${ex.getCause()}")
                    p (class:'text-primary'," MESSAGE: ${ex.getMessage()}")
                    p (class:'text-primary'," MESSAGE: ${ex.getLocalizedMessage()}")
                    table(class: 'table table-bordered table-hover table-condensed') {
                        thead {
                            tr {
                                th('#')
                                th('StackTrace')
                            }
                        }
                        tbody {
                            def i = 1

                            ex.getStackTrace().toList().each { errorUnit ->
                                tr(class: 'danger') {
                                    td(class: '', "${i++}")
                                    td(class: '', "${errorUnit}")
                                }
                            }
                        }
                    }
                }
            }
        }