package templates.views

layout 'layouts/main.tpl',
        filesList: logData.fileSet,
        mainBody: contents {
            div(class: 'container') {
                form(method: 'POST', enctype: 'multipart/form-data', action: '/files') {
                    div(class: 'form-group') {
                        label(class: 'btn btn-primary') {
                            yield 'Select file...'
                            input(type: 'file', name: 'file', style: 'display: none;')
                        }
                    }
                    div(class: 'form-group') {
                        button(type: 'submit', class: 'btn btn-success', id: 'sendFile', 'Upload')

                    }
                }
                if ("${message}" != 'null') {
                    div(class: 'alert alert-success') {
                        strong("${message}")
                    }
                }
                div(class:'list-group'){
                    a(href:'/errors/allErrors',class:'list-group-item'){
                        yeld 'Errors : '
                        strong("${logData.errors.size()}")
                    }
                    a(href:'/fares/fromFS',class:'list-group-item'){
                        yeld 'Fares from FS: '
                        strong("${logData.faresFromFS.size()}")
                    }
                    a(href:'/fares/removed',class:'list-group-item'){
                        yeld 'Removed fares : '
                        strong("${logData.removedFares.size()}")
                    }
                    a(href:'/alfs/RS',class:'list-group-item'){
                        yeld 'Generated itineraries : '
                        strong("${logData.pricedItineraries.size()}")
                    }
                    a(href:'/avail/allAvails',class:'list-group-item'){
                        yeld 'Avails : '
                        strong("${logData.avails.size()}")
                    }

                    a(href:'/service/getAll',class:'list-group-item'){
                        yeld 'Services calls : '
                        strong("${logData.serviceRecords.size()}")
                    }
                    a(href:'/taxes/applicableTaxes',class:'list-group-item'){
                        yeld 'Added Taxes : '
                        strong("${logData.addedTaxes.size()}")
                    }
                }

            }
        }