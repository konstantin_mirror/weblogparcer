package templates.views

layout 'layouts/main.tpl',
        filesList : files,
        mainBody: contents {
            div(class: 'container') {
                table(class: 'table table-bordered table-hover table-condensed') {
                    thead {
                        tr {
                            th('#')
                            th('Code')
                            th('Nation')
                            th('Type')
                            th('SeqNo')
                        }
                    }
                    tbody {

                        def i = 1
                        taxes.each { tax ->
                            tr {
                                td(class: '', "${i++}")
                                td(class: '', "${tax.code}")
                                td(class: '', "${tax.nation}")
                                td(class: '', "${tax.type}")
                                td(class: '', "${tax.seqNo}")

                            }
                        }
                    }
                }
            }
        }