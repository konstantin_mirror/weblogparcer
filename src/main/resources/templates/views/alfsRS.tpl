package templates.views

layout 'layouts/main.tpl',
        filesList: files,
        mainBody: contents {
            div(class: 'container') {
                table(class: 'table table-hover table-bordered table-condensed') {
                    tbody() {
                        itineraries.each { itinerary ->
                            tr() {
                                td() {
                                    div(class: 'alert alert-success') {
                                        div() {
                                            yeld 'Sequence Number : '
                                            strong("${itinerary.sequenceNumber}")
                                        }
                                        div() {
                                            yeld 'origin Destination RPH : '
                                            strong("${itinerary.originDestinationRPH}")
                                        }
                                    }
                                    table(class: 'table table-bordered table-condensed') {
                                        thead {
                                            tr {
                                                th('Class')
                                                th('Tariff')
                                                th('Rule')
                                                th('ID')
                                                th('Origin')
                                                th('Dest')
                                                th('Routing')
                                                th('Amount')
                                                th('Currency')
                                                th('Fare Family')
                                            }
                                        }
                                        tbody {

                                            itinerary.fares.each { fare ->
                                                tr {
                                                    td(class: '', "${fare.clazz}")
                                                    td(class: '', "${fare.tariff}")
                                                    td(class: '', "${fare.rule}")
                                                    td(class: '', "${fare.id}")
                                                    td(class: '', "${fare.origin}")
                                                    td(class: '', "${fare.destination}")
                                                    td(class: '', "${fare.routing}")
                                                    td(class: '', "${fare.amount}")
                                                    td(class: '', "${fare.currency}")
                                                    td(class: '', "${fare.fareFamily}")
                                                }
                                            }
                                        }
                                    }
                                    table(class: 'table table-bordered table-condensed') {
                                        thead {
                                            tr {
                                                th('Departure Airport')
                                                th('Arrival Airport')
                                                th('Flight Number')
                                                th('Marketing Airline')
                                                th('Operating Airline')
                                                th('Departure Date')
                                                th('Arrival Date')
                                                th('RBD')
                                                th('Quantity RBD')
                                            }
                                        }
                                        tbody {
                                            itinerary.avails.each { availUnit ->
                                                tr {
                                                    td(class: '', "${availUnit.departureAirport}")
                                                    td(class: '', "${availUnit.arrivalAirport}")
                                                    td(class: '', "${availUnit.flightNumber}")
                                                    td(class: '', "${availUnit.marketingAirline}")
                                                    td(class: '', "${availUnit.operatingAirline}")
                                                    td(class: '', "${availUnit.departureDateTime}")
                                                    td(class: '', "${availUnit.arrivalDateTime}")
                                                    td(class: '', "${availUnit.requiredRBD}")
                                                    td(class: '', "${availUnit.getRequredRBDQuantity()} ")
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }