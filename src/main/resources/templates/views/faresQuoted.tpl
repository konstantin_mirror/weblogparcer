package templates.views

layout 'layouts/main.tpl',
        filesList: files,
        mainBody: contents {
            div(class: 'container') {
                div(class: 'form-group row') {
                    div(class: 'col-xs-2') {
                        label(for: 'ex1', 'Class:')
                        input(class: 'form-control', id: 'ex1', type: 'text')
                    }
                    div(class: 'col-xs-2') {
                        label(for: 'ex2', 'Tariff:')
                        input(class: 'form-control', id: 'ex2', type: 'text')
                    }
                    div(class: 'col-xs-2') {
                        label(for: 'ex3', 'Rule:')
                        input(class: 'form-control', id: 'ex3', type: 'text')
                    }
                }
                table(class: 'table table-bordered table-hover table-condensed') {
                    thead {
                        tr {
                            th('#')
                            th('CAT25')
                            th('Constr')
                            th('Class')
                            th('Tariff')
                            th('Rule')
                            th('Origin')
                            th('Destination')
                            th('Fare Family')
                            th('ID')
                            th('Amount')
                            th('Currency')
                        }
                    }
                    tbody {
                        def i = 1
                        fares.each { fare ->
                            tr {
                                td(class: '', "${i++}")
                                if (fare.isCat25()) {
                                    td(class: '') {
                                        span(class: 'glyphicon glyphicon-ok')
                                    }
                                } else {
                                    td(class: '') {
                                        span(class: 'glyphicon glyphicon-remove')
                                    }
                                }
                                if (fare.isConstructed()) {
                                    td(class: '') {
                                        span(class: 'glyphicon glyphicon-ok')
                                    }
                                } else {
                                    td(class: '') {
                                        span(class: 'glyphicon glyphicon-remove')
                                    }
                                }
                                td(class: 'info', "${fare.clazz}")
                                td(class: '', "${fare.tariff}")
                                td(class: '', "${fare.rule}")
                                td(class: '', "${fare.origin}")
                                td(class: '', "${fare.destination}")
                                td(class: '', "${fare.fareFamily}")
                                td(class: '', "${fare.id}")
                                td(class: '', "${fare.amount}")
                                td(class: '', "${fare.currency}")
                            }
                        }
                    }
                }
            }
        }



