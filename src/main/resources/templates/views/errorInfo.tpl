package templates.views

layout 'layouts/main.tpl',
        filesList: files,
        mainBody: contents {

            table(class: 'table table-condensed') {
                thead {
                    tr {
                        th('#')
                        th('Message')
                    }
                }
                tbody {

                    def i = 1
                    errorsList.each { errorUnit ->
                        tr(class: 'warning') {
                            td(class: 'col-sm-1', "${i++}")
                            td {
                                div() {
                                    yield("${errorUnit}")
                                }

                            }
                        }
                    }
                }
            }

        }