package templates.views

import com.epam.loganalyzer.model.record.ServiceRecord

layout 'layouts/main.tpl',
        filesList: files,
        mainBody: contents {
            div(class: 'container') {
                form(method: 'POST', action: '/service/files/collectedServices') {
                    div(class: 'form-group') {
                        button(type: 'submit', class: 'btn btn-success', 'Upload')

                    }
                    table(class: 'table table-bordered table-hover table-condensed') {
                        thead {
                            tr {
                                th('#')
                                th()
                                th('Time')
                                th('Thread')
                                th('Type')
                                th('Name')
                                th("Client ID")
                                th("Unique ID")
                                th('Message')
                                th('View')
                            }
                        }
                        tbody {
                            def i = 1
                            records.each { record ->
                                def isWarning = ''
                                def isService = record in ServiceRecord
                                if (record.timeStamp.type == "ERROR" ) {
                                    isWarning = 'danger'
                                }else if(isService && record.containError){
                                    isWarning = 'warning'
                                }
                                if ((record.timeStamp.type == "DEBUG" || record.timeStamp.type == "INFO ") &&
                                        !isService) {
                                    isWarning = 'success'
                                }

                                tr(class: "${isWarning}") {
                                    td(class: 'col-sm-1', "${i++}")
                                    if (isService) {
                                        td() {
                                            input(type: 'checkbox', name: 'servicesNames', value: "${record.name}!${record.timeStamp.time}")
                                        }
                                    } else {
                                        td()
                                    }

                                    td("${record.timeStamp.time}")
                                    td("${record.timeStamp.thread}")
                                    td("${record.timeStamp.type}")
                                    if (isService) {
                                        td("${record.name}")
                                        td("${record.clientId}")
                                        td("${record.uniqueId}")
                                    } else {
                                        td()
                                        td()
                                        td()
                                    }
                                    td("${record.massage}")
                                    if (isService) {
                                        td() {
                                            a(href: "/service/files/${record.name}!${record.timeStamp.time}") {
                                                button(type: 'button', class: 'btn btn-primary btn-xs', 'View')
                                            }
                                        }
                                    } else if (!isService && record.timeStamp.type == "ERROR") {
                                        td(class: '') {
                                            a(href: "/errors/error?time=${record.timeStamp.time}&" +
                                                    "thread=${record.timeStamp.thread}&" +
                                                    "type=${record.timeStamp.type}&" +
                                                    "clazz=${record.timeStamp.clazz}&" +
                                                    "massage=${record.massage}") {
                                                button(type: 'button', class: 'btn btn-primary btn-xs', 'View')
                                            }
                                        }
                                    } else {
                                        td()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }