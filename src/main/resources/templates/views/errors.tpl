package templates.views

layout 'layouts/main.tpl',
        filesList: files,
        mainBody: contents {

            table(class: 'table table-striped') {
                thead {
                    tr {
                        th('#')
                        th('Time')
                        th('Thread')
                        th('Type')
                        th('Class')
                        th('message')
                        th('More info')
                    }
                }
                tbody {
                    def i = 1
                    errorsList.each { errorUnit ->
                        tr(class: 'danger') {
                            td(class: '', "${i++}")
                            td(class: '', "${errorUnit.timeStamp.time}")
                            td(class: '', "${errorUnit.timeStamp.thread}")
                            td(class: '', "${errorUnit.timeStamp.type}")
                            td(class: '', "${errorUnit.timeStamp.clazz}")
                            td(class: 'col-sm-2') {
                                yield("${errorUnit.massage}")
                            }
                            td(class: '') {
                                a(href: "/errors/error?time=${errorUnit.timeStamp.time}&" +
                                        "thread=${errorUnit.timeStamp.thread}&" +
                                        "type=${errorUnit.timeStamp.type}&" +
                                        "clazz=${errorUnit.timeStamp.clazz}&") {
                                    button(type: 'button', class: 'btn btn-primary btn-xs', 'View')
                                }
                            }
                        }
                    }
                }
            }
        }