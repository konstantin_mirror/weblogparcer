package templates.views

layout 'layouts/main.tpl',
        filesList: files,
        mainBody: contents {
            div(class: 'container') {

                table(class: 'table table-condensed') {
                    thead {
                        tr {
                            th('#')
                            th('Message')
                            th('Quantity')
                            th('More info')
                        }
                    }
                    tbody {

                        def i = 1
                        massagesStatistic.keySet().each { errorUnit ->
                            tr(class: 'warning') {
                                td(class: 'col-sm-1', "${i++}")
                                td() {
                                    yield("${errorUnit}")
                                }
                                td() {
                                    yield("${massagesStatistic.get(errorUnit)}")
                                }
                                td(class: '') {
                                    a(href: "/errors/specificErrors?massage=${errorUnit}")
                                            {
                                                button(type: 'button', class: 'btn btn-primary btn-xs', 'View')
                                            }
                                }
                            }
                        }
                    }
                }

                table(class: 'table table-condensed') {
                    thead {
                        tr {
                            th('#')
                            th('Class')
                            th('Quantity')
                            th('More info')
                        }
                    }
                    tbody {

                        def i = 1
                        classesStatistic.keySet().each { errorUnit ->
                            tr(class: 'warning') {
                                td(class: 'col-sm-1', "${i++}")
                                td() {
                                    yield("${errorUnit}")
                                }
                                td() {
                                    yield("${classesStatistic.get(errorUnit)}")
                                }
                                td(class: '') {
                                    a(href: "/errors/specificErrors?clazz=${errorUnit}")
                                            {
                                                button(type: 'button', class: 'btn btn-primary btn-xs', 'View')
                                            }
                                }
                            }
                        }
                    }
                }

            }
        }