package templates.views

layout 'layouts/main.tpl',
        filesList: files,
        mainBody: contents {
            div(class: 'container') {
                div(class: 'container') {
                    form(method: 'POST', action: '/fares/removed') {
                        div(class: 'form-group row') {
                            div(class: 'col-xs-2') {
                                label(for: 'ex1', 'Class:')
                                input(class: 'form-control', name: 'clazz', id: 'ex1', type: 'text', value: "${clazz == null ? "" : clazz}")
                            }
                            div(class: 'col-xs-2') {
                                label(for: 'ex2', 'Tariff:')
                                input(class: 'form-control', name: 'tariff', id: 'ex2', type: 'text',value: "${tariff == null ? "" : tariff}")
                            }
                            div(class: 'col-xs-2') {
                                label(for: 'ex3', 'Rule:')
                                input(class: 'form-control', name: 'rule', id: 'ex3', type: 'text',value: "${rule == null ? "" : rule}")
                            }
                            div(class: 'col-xs-2') {
                                label(for: 'ex4', 'ID:')
                                input(class: 'form-control', name: 'id', id: 'ex4', type: 'text',value: "${id == null ? "" : id}")
                            }
                            div(class: 'col-xs-1') {
                                label(for: 'ex5', 'Search.. ')
                                button(class: 'form-control btn btn-primary', type: 'submit', id: 'ex5', 'Search')
                            }
                        }
                    }
                    table(class: 'table table-bordered table-hover table-condensed') {
                        thead {
                            tr {
                                th('#')
                                th('CAT25')
                                th('Constr')
                                th('Class')
                                th('Tariff')
                                th('Rule')
                                th('ID')
                                th('More info')
                            }
                        }
                        tbody {

                            def i = 1
                            fares.each { fare ->
                                tr {
                                    td(class: '', "${i++}")
                                    if (fare.isCat25()) {
                                        td(class: '') {
                                            span(class: 'glyphicon glyphicon-ok')
                                        }
                                    } else {
                                        td(class: '') {
                                            span(class: 'glyphicon glyphicon-remove')
                                        }
                                    }
                                    if (fare.isConstructed()) {
                                        td(class: '') {
                                            span(class: 'glyphicon glyphicon-ok')
                                        }
                                    } else {
                                        td(class: '') {
                                            span(class: 'glyphicon glyphicon-remove')
                                        }
                                    }
                                    td(class: '', "${fare.clazz}")
                                    td(class: '', "${fare.tariff}")
                                    td(class: '', "${fare.rule}")
                                    td(class: '', "${fare.id}")
                                    td(class: '') {
                                        a(href: "/fares/fare?clazz=${fare.clazz}&tariff=${fare.tariff}&rule=${fare.rule}&ID=${fare.id}") {
                                            button(type: 'button', class: 'btn btn-primary btn-xs', 'View')
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


