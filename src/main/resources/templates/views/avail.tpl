package templates.views

layout 'layouts/main.tpl',
        filesList: files,
        mainBody: contents {

            div(class: 'container') {
                div(class: 'form-group row') {
                    div(class: 'col-xs-2') {
                        label(for: 'ex1', 'Origin:')
                        input(class: 'form-control', id: 'ex1', type: 'text')
                    }
                    div(class: 'col-xs-2') {
                        label(for: 'ex2', 'Destination:')
                        input(class: 'form-control', id: 'ex2', type: 'text')
                    }
                    div(class: 'col-xs-2') {
                        label(for: 'ex3', 'Flight Number:')
                        input(class: 'form-control', id: 'ex3', type: 'text')
                    }
                }
                avails.each { availUnit ->
                    div(class: 'alert alert-info') {
                        div() {
                            yeld 'Departure Airport : '
                            strong("${availUnit.departureAirport}")
                        }
                        div() {
                            yeld 'Arrival Airport : '
                            strong("${availUnit.arrivalAirport}")
                        }
                        div() {
                            yeld 'Flight Number : '
                            strong("${availUnit.flightNumber}")
                        }
                        div() {
                            yeld 'Marketing Airline : '
                            strong("${availUnit.marketingAirline}")
                        }
                        div() {
                            yeld 'Operating Airline : '
                            strong("${availUnit.operatingAirline}")
                        }
                        div() {
                            yeld 'Departure Date Time : '
                            strong("${availUnit.departureDateTime}")
                        }
                        div() {
                            yeld 'Arrival Date Time : '
                            strong("${availUnit.arrivalDateTime}")
                        }
                    }
                    table(class: 'table table-hover table-condensed') {
                        thead {
                            tr {
                                th('#')
                                th('Cabin Code')
                                th('Cabin Description')
                                th('RBD')
                                th('RBD Quantity')
                            }
                        }
                        tbody {
                            def i = 1
                            availUnit.bookingClassAvailabilities.each { info ->
                                def isWorning = ''
                                if (info.RBDQuantity == '000' || info.RBDQuantity == 'SoldOut') {
                                    isWorning = 'danger'
                                }
                                tr(class: "${isWorning}") {
                                    td(class: '', "${i++}")
                                    td(class: '', "${info.cabinCode}")
                                    td(class: '', "${info.cabinDescription}")
                                    td(class: '', "${info.RBD}")
                                    td(class: '', "${info.RBDQuantity}")
                                }

                            }
                        }
                    }
                }
            }
        }