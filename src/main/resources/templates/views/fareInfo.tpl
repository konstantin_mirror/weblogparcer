package templates.views

layout 'layouts/main.tpl',
        filesList: files,
        mainBody: contents {

            table(class: 'table table-bordered table-condensed') {
                thead {
                    tr {
                        th('Class')
                        th('message')
                    }
                }
                tbody {

                    def i = 1
                    info.each { fare ->
                        tr {
                            td(class: '', "${i++}")
                            td {
                                fare.each { line ->
                                    div("${line}")
                                }
                            }
                        }
                    }
                }
            }

        }