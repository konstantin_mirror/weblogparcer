yieldUnescaped '<!DOCTYPE html>'
html(lang: 'en') {
    head {
        title('Log Analysis')
        meta(charset: 'utf-8')
        meta(name: 'viewport', content: 'width=device-width,initial-scale=1')
        link(rel: 'stylesheet', href: 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css')
        link(rel: 'stylesheet', href: '/css/custom-styles.css')
        yieldUnescaped '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>'
        yieldUnescaped '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>'
    }
    body {
        nav(class: 'navbar navbar-inverse') {
            div(class: 'container-fluid') {
                div(class: 'navbar-header') {
                    a(class: 'navbar-brand', href: '#', '#####')
                }
                ul(class: 'nav navbar-nav') {
                    li(class: 'active') {
                        a(href: '/home', 'Home')
                    }
                    li(class: 'dropdown active') {
                        a(class: 'dropdown-toggle', 'data-toggle': 'dropdown', href: '#') {
                            yield 'Fares Information'
                            span(class: 'caret')
                        }
                        ul(class: 'dropdown-menu active') {
                            li {
                                a(href: "/fares/removed", "Removed fares")
                            }
                            li {
                                a(href: "/fares/faresQuoted", "Quoted fares")
                            }
                            li {
                                a(href: "/fares/fromFS", "Fares from FS")
                            }
                        }
                    }
                    li(class: 'dropdown active') {
                        a(class: 'dropdown-toggle', 'data-toggle': 'dropdown', href: '#') {
                            yield 'Taxes Information'
                            span(class: 'caret')
                        }
                        ul(class: 'dropdown-menu active') {
                            li {
                                a(href: "/taxes/applicableTaxes", "Applicable taxes")
                            }
                        }
                    }
                    li(class: 'dropdown active') {
                        a(class: 'dropdown-toggle', 'data-toggle': 'dropdown', href: '#') {
                            yield 'Services Call Information'
                            span(class: 'caret')
                        }
                        ul(class: 'dropdown-menu active') {
                            li {
                                a(href: "/service/getAll", "Services")
                            }
                            li {
                                a(href: "/service/getAll?info=true", "Services + Info")
                            }
                            li {
                                a(href: "/service/getAll?info=true&error=true", "Services + Info + Error")
                            }
                        }
                    }

                    li(class: 'dropdown active') {
                        a(class: 'dropdown-toggle', 'data-toggle': 'dropdown', href: '#') {
                            yield 'Errors Information'
                            span(class: 'caret')
                        }
                        ul(class: 'dropdown-menu active') {
                            li {
                                a(href: "/errors/allErrors", "All Errors")
                            }
                            li {
                                a(href: "/errors/Statistic", "Statistic Errors")
                            }
                        }
                    }

                    li(class: 'active') {
                        a(href: '/avail/allAvails', 'Avails')
                    }
                    li(class: 'active') {
                        a(href: '/alfs/RS', 'ALFS')
                    }
                    li(class: 'dropdown active') {
                        a(class: 'dropdown-toggle', 'data-toggle': 'dropdown', href: '#') {
                            yield 'Downloads'
                            span(class: 'caret')
                        }

                        ul(class: 'dropdown-menu active') {
                            filesList.each { file ->
                                li {
                                    a(href: "/files/${file}", "${file}")
                                }
                            }
                        }
                    }
                }
            }
        }
        mainBody()
    }
}
